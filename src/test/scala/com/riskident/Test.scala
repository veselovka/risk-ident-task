package com.riskident

import java.nio.file.Paths

import org.scalatest.FunSuite
import org.scalatest.Matchers._

class Test extends FunSuite {

  test("on empty input") {
    runTestOn("empty-input.txt", 0)
  }

  test("on input1") {
    runTestOn("input1.txt", 9)
  }

  test("on input2") {
    runTestOn("input2.txt", 8)
  }

  test("on input3") {
    runTestOn("input3.txt", 6)
  }

  test("on input4") {
    runTestOn("input4.txt", 1418670047)
  }

  test("on input5") {
    runTestOn("input5.txt", BigInt(8485548331L))
  }
  
  private def runTestOn(resource: String, expected: BigInt) = {
    val path = testResource(resource)
    val orders = DataParser.parse(path)
    val actual = new ProblemSolver(orders).minAvgWaitTime()
    actual should be (expected)
  }

  private def testResource(name: String) =
    Paths.get(getClass.getResource(s"/$name").toURI)
}
