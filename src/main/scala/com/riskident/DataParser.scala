package com.riskident

import java.nio.file.Path

import scala.io.Source

object DataParser {

  def parse(): Seq[Order] = {
    mapInput(Source.stdin.getLines())
  }

  def parse(path: Path): Seq[Order] = {
    mapInput(Source.fromFile(path.toFile).getLines())
  }

  private def mapInput(lines: Iterator[String]): Seq[Order] = {
    val n = if (lines.hasNext) lines.next().toInt else 0
    (1 to n).map { _ ⇒
      if (lines.hasNext) {
        val line = lines.next()
        val nums = line.split(' ')
        require(nums.length == 2, "Illegal input line format")
        Order(nums.head.toInt, nums(1).toInt)
      } else throw new IllegalArgumentException("Not enough input lines")
    }.toSeq.sortBy(_.time)
  }
}
