package com.riskident

import java.nio.file.Paths

object MinAverageWaitingTimeApp extends App {
  val orders = args.headOption.map(path ⇒ DataParser.parse(Paths.get(path))).getOrElse(DataParser.parse())
  val result = new ProblemSolver(orders).minAvgWaitTime()
  println(s"Answer: $result")
}
