package com.riskident

import scala.collection.mutable.{PriorityQueue ⇒ mPQ}

class ProblemSolver(orders: Seq[Order]) {

  private val waitingOrders = new mPQ[Order]()(Ordering.fromLessThan(_.pizzaTime > _.pizzaTime))
  private var receivedOrders = Seq.empty[Order]
  private var futureOrders = orders
  private var currTime = BigInt(0)
  private var waitTime = BigInt(0)
  private var n = 0

  def minAvgWaitTime(): BigInt = {
    def ordersExist = {
      receivedOrders.nonEmpty || futureOrders.nonEmpty || waitingOrders.nonEmpty
    }

    def processNextOrder() = {
      if (receivedOrders.isEmpty && waitingOrders.isEmpty) {
        currTime = futureOrders.head.time // tick to next order time
      } else {
        waitingOrders ++= receivedOrders
        receivedOrders = Seq.empty
        val order = waitingOrders.dequeue()
        val orderWaitTime = currTime - order.time + order.pizzaTime
        waitTime = waitTime + orderWaitTime
        currTime = currTime + order.pizzaTime
        n = n + 1
      }
    }

    while (ordersExist) {
      val (received, future) = futureOrders.span(_.time <= currTime)
      receivedOrders = received
      futureOrders = future
      processNextOrder()  
    }
    if (n != 0) waitTime / n else 0
  }
}
